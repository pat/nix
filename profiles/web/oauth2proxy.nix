{config, ...}: {
  age.secrets.oauthProxy = {
    file = ../../secrets/oauthProxy.age;
  };

  services.oauth2_proxy = {
    enable = true;
    reverseProxy = true;

    redirectURL = "https://oauthproxy.tardisproject.uk/oauth2/callback";
    email.domains = ["*"];
    cookie = {
      domain = "tardisproject.uk";
    };

    provider = "keycloak-oidc";
    clientID = "oauth2proxy";
    extraConfig = {
      "oidc-issuer-url" = "https://id.tardisproject.uk/realms/master";
      "session-store-type" = "redis";
      "redis-connection-url" = "unix://${config.services.redis.servers.oauthproxy.unixSocket}";
      "whitelist-domain" = "*.tardisproject.uk";
    };
    keyFile = config.age.secrets.oauthProxy.path;
  };

  services.redis.servers.oauthproxy = {
    enable = true;
    user = "oauth2-proxy";
    save = [];
  };
}
