{
  pkgs,
  config,
  ...
}: {
  age.secrets.mktxpConfig = {
    file = ../../secrets/mktxpConfig.age;
    path = "/etc/mktxp";
    owner = "100";
  };

  # Python developers do not deserve rights ong
  virtualisation.oci-containers.containers.mktxp = {
    image = "ghcr.io/akpw/mktxp:gha-5473726165";
    ports = ["127.0.0.1:49090:49090"];
    volumes = [
      "${config.age.secrets.mktxpConfig.path}:/home/mktxp/mktxp/mktxp.conf:U,ro"
      "${pkgs.writeText "_mktxp.conf" ''
        [MKTXP]
            port = 49090
            socket_timeout = 2

            initial_delay_on_failure = 120
            max_delay_on_failure = 900
            delay_inc_div = 5

            bandwidth = False                # Turns metrics bandwidth metrics collection on / off
            bandwidth_test_interval = 600    # Interval for colllecting bandwidth metrics
            minimal_collect_interval = 5     # Minimal metric collection interval

            verbose_mode = False             # Set it on for troubleshooting

            fetch_routers_in_parallel = False   # Set to True if you want to fetch multiple routers parallel
            max_worker_threads = 5              # Max number of worker threads that can fetch routers (parallel fetch only)
            max_scrape_duration = 10            # Max duration of individual routers' metrics collection (parallel fetch only)
            total_max_scrape_duration = 30      # Max overall duration of all metrics collection (parallel fetch only)
      ''}:/home/mktxp/mktxp/_mktxp.conf"
    ];
  };

  services.netdata.prometheusScrapers = [
    ''
      - name: microtik
        url: http://127.0.0.1:49090/metrics
        selector:
          allow:
            - mktxp_interface_rx_byte_total
            - mktxp_interface_tx_byte_total
            - mktxp_interface_rx_drop_total
            - mktxp_interface_tx_drop_total
            - mktxp_link_downs_total
            - mktxp_system_cpu_load
            - mktxp_system_free_hdd_space
            - mktxp_system_routerboard_temperature
            - mktxp_system_uptime
    ''
  ];
}
