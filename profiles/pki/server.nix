{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./client.nix
  ];

  environment.systemPackages = [pkgs.step-cli];

  age.secrets.smallstepPassword.file = ../../secrets/smallstepPassword.age;
  services.step-ca = {
    enable = true;
    openFirewall = true;
    intermediatePasswordFile = config.age.secrets.smallstepPassword.path;
    address = "0.0.0.0";
    port = 443;

    settings = builtins.fromJSON ''
      {
        "root": "/var/lib/step-ca/certs/root_ca.crt",
        "federatedRoots": null,
        "crt": "/var/lib/step-ca/certs/intermediate_ca.crt",
        "key": "/var/lib/step-ca/secrets/intermediate_ca_key",
        "address": ":443",
        "insecureAddress": "",
        "dnsNames": [
          "ca.tardisproject.uk",
          "enclave.tardisproject.uk"
        ],
        "logger": {
          "format": "text"
        },
        "db": {
          "type": "badgerv2",
          "dataSource": "/var/lib/step-ca/db",
          "badgerFileLoadingMode": ""
        },
        "authority": {
          "provisioners": [
            {
                "type": "ACME",
                "name": "acme"
            },
            {
                "type": "OIDC",
                "name": "Tardis Account",
                "clientID": "ca",
                "clientSecret": "4ARqmXZKIKcIQww6Rk4Ko8lIMTIeJmxd",
                "configurationEndpoint": "https://id.tardisproject.uk/realms/master/.well-known/openid-configuration",
                "domains": [
                    "tardisproject.uk"
                ],
                "admins": [
                    "sysmans@tardisproject.uk",
                    "tcmal@tardisproject.uk"
                ],
                "claims": {
                    "enableSSHCA": true,
                    "disableRenewal": false,
                    "allowRenewalAfterExpiry": false
                },
                "options": {
                    "x509": {},
                    "ssh": {}
                }
            }
          ]
        },
        "tls": {
          "cipherSuites": [
            "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256",
            "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"
          ],
          "minVersion": 1.2,
          "maxVersion": 1.3,
          "renegotiation": false
        }
      }
    '';
  };
}
