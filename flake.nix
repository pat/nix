{
  description = "Nix configurations for Tardis project core services.";

  inputs = {
    nixos.url = "github:nixos/nixpkgs/nixos-24.05";
    nixos-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    agenix = {
      url = "github:ryantm/agenix";

      inputs = {
        nixpkgs.follows = "nixos";
        darwin.follows = "";
        home-manager.follows = "";
      };
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixos";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixos";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixos";
    };
  };

  outputs = inputs @ {
    nixos,
    nixos-unstable,
    agenix,
    devshell,
    rust-overlay,
    ...
  }: let
    system = "x86_64-linux";
    overlays = [
      agenix.overlays.default
      devshell.overlays.default
      rust-overlay.overlays.default

      # Access helpful variables from nixpkgs
      (_: prev: prev // {inherit inputs overlays;})

      # Add our custom library functions
      (final: prev: prev // {lib = prev.lib // import ./lib.nix final;})

      # Access to unstable packages
      (final: prev:
        prev
        // {
          unstable = import nixos-unstable {
            system = final.system;
            config.allowUnfree = true;
          };
        })

      # FIXME: Tests for the current smallstep nix package are broken :(
      (final: prev:
        prev
        // {
          step-ca = prev.step-ca.overrideAttrs (old: {
            doCheck = false;
          });
        })

      # HACK: Upgrade netdata & use cloud dashboard
      (final: prev:
        prev
        // {
          netdata = prev.unstable.netdata.override {
            withCloud = false;
            withCloudUi = true;
          };
        })

      # Add our packages
      (import ./pkgs)
    ];
    pkgs = import nixos {
      inherit system overlays;
    };
    lib = pkgs.lib;
    hosts = {
      monitoring = ./hosts/monitoring.nix;
      web = ./hosts/web.nix;
      enclave = ./hosts/enclave.nix;
    };
  in {
    devShells.${system} = {
      default = import ./shells/default.nix pkgs;
      rust = import ./shells/rust.nix pkgs;
    };
    packages.${system} = {
      # Colmena is exposed here so we can use it in CI easier
      inherit (pkgs) sonic-screwdriver colmena;
    };

    nixosConfigurations = builtins.mapAttrs (_: p: lib.mkSystem p) hosts;

    colmena =
      {
        meta = {
          nixpkgs = pkgs;
          specialArgs = {inherit (pkgs) lib;};
        };
      }
      // (builtins.mapAttrs (name: path: lib.mkColmenaNode name path)) hosts;
  };
}
