# TODO: This only does renewals. There should be some docs on the initial setup required,
# Mostly permissions
{
  config,
  lib,
  pkgs,
  options,
  ...
}:
with lib; let
  cfg = config.security.smallstep;
  certToConfig = cert: data: let
  in {
    inherit cert;

    group = data.group;

    renewTimer = {
      description = "Renew smallstep Certificate for ${cert}";
      wantedBy = ["timers.target"];
      timerConfig = {
        Persistent = "yes";
        OnCalendar = "*:1/15";
        AccuracySec = "1us";
        Unit = "smallstep-${cert}.service";
        RandomizedDelaySec = "5m";
      };
    };

    renewService = {
      description = "Renew smallstep certificate for ${cert}";
      after = ["network.target" "network-online.target" "nss-lookup.target"];
      wants = ["network-online.target"];

      path = with pkgs; [step-cli coreutils];
      serviceConfig = {
        Group = data.group;

        User = "smallstep";
        # Run as root (Prefixed with +)
        ExecStartPost =
          "+"
          + (pkgs.writeShellScript "smallstep-postrun" ''
            cd /var/lib/smallstep/${escapeShellArg cert}
            chmod 640 ./*;
            if [ -e renewed ]; then
              rm renewed
              ${
              optionalString (data.reloadServices != [])
              "systemctl --no-block try-reload-or-restart ${escapeShellArgs data.reloadServices}"
            }
            fi
          '');
      };

      script = ''
        set -xu
        set +e
        cd /var/lib/smallstep/${escapeShellArg cert}
        step certificate needs-renewal \
            cert.pem \
            --expires-in 50%
        status=$?
        set -e
        if [ $status -eq 1 ]; then
            echo "Certificate does not need renewal"
        elif [ $status -eq 0 ]; then
            echo "Renewing certificate"
            step ca renew --force \
                --ca-url ${escapeShellArg data.caURL} \
                --root ${escapeShellArg data.caRootPath} \
                cert.pem \
                key.pem
        else
            echo "Unknown error"
            exit 1
        fi
      '';
    };
  };

  certConfigs = mapAttrs certToConfig cfg.certs;
  inheritableModule = isDefaults: {config, ...}: let
    defaultAndText = name: default: {
      default =
        if isDefaults || ! config.inheritDefaults
        then default
        else cfg.defaults.${name};
      defaultText =
        if isDefaults
        then default
        else literalExpression "config.security.smallstep.defaults.${name}";
    };
  in {
    options = {
      caURL = mkOption {
        type = types.str;
        inherit (defaultAndText "caURL" null) default defaultText;
        description = lib.mdDoc ''
          CA domain.
        '';
      };

      caRootPath = mkOption {
        type = with types; oneOf [str path];
        inherit (defaultAndText "caRootPath" null) default defaultText;
        description = lib.mdDoc ''
          Path of the root CA certificate
        '';
      };

      group = mkOption {
        type = types.str;
        inherit (defaultAndText "group" "smallstep") default defaultText;
        description = lib.mdDoc "Group running the SMALLSTEP client.";
      };

      reloadServices = mkOption {
        type = types.listOf types.str;
        inherit (defaultAndText "reloadServices" []) default defaultText;
        description = lib.mdDoc ''
          The list of systemd services to call `systemctl try-reload-or-restart`
          on.
        '';
      };

      postRun = mkOption {
        type = types.lines;
        inherit (defaultAndText "postRun" "") default defaultText;
        example = "cp full.pem backup.pem";
        description = lib.mdDoc ''
          Commands to run after new certificates go live. Note that
          these commands run as the root user.

          Executed in the same directory with the new certificate.
        '';
      };
    };
  };

  certOpts = {name, ...}: {
    options = {
      directory = mkOption {
        type = types.str;
        readOnly = true;
        default = "/var/lib/smallstep/${name}";
        description = lib.mdDoc "Directory where certificate and other state is stored.";
      };

      inheritDefaults = mkOption {
        default = true;
        example = true;
        description = lib.mdDoc "Whether to inherit values set in `security.smallstep.defaults` or not.";
        type = lib.types.bool;
      };
    };
  };
in {
  options = {
    security.smallstep = {
      defaults = mkOption {
        type = types.submodule (inheritableModule true);
        description = lib.mdDoc ''
          Default values inheritable by all configured certs. You can
          use this to define options shared by all your certs. These defaults
          can also be ignored on a per-cert basis using the
          `security.smallstep.certs.''${cert}.inheritDefaults' option.
        '';
      };

      certs = mkOption {
        default = {};
        type = with types; attrsOf (submodule [(inheritableModule false) certOpts]);
        description = lib.mdDoc ''
          Attribute set of certificates to get signed and renewed.
        '';
      };
    };
  };

  config = mkMerge [
    (mkIf (cfg.certs != {}) {
      users.users.smallstep = {
        home = "/var/lib/smallstep";
        group = "smallstep";
        isSystemUser = true;
      };
      users.groups.smallstep = {};

      systemd.services = mapAttrs' (cert: conf: nameValuePair "smallstep-${cert}" conf.renewService) certConfigs;

      systemd.timers = mapAttrs' (cert: conf: nameValuePair "smallstep-${cert}" conf.renewTimer) certConfigs;

      systemd.targets = mapAttrs' (cert: _:
        nameValuePair "smallstep-finished-${cert}" {
          wantedBy = ["default.target"];
          requires = ["smallstep-${cert}.service"];
          after = ["smallstep-${cert}.service"];
        })
      certConfigs;
    })
  ];
}
