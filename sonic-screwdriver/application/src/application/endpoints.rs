use anyhow::{anyhow, bail, Result};

use crate::{adapters::DBError, App};
use sonic_types::{
    endpoint::{AddDomain, Domain, Endpoint, NewEndpoint},
    Username,
};

use super::errors::{log_hidden_err, HideErrorExt};

impl App {
    pub async fn endpoints_for_user(&self, uid: &Username) -> Result<Vec<Endpoint>> {
        self.endpoints
            .get_by_user(uid)
            .await
            .map(Into::into)
            .hide_err("Error getting endpoints for user")
    }

    pub async fn domains_for_user(&self, uid: &Username) -> Result<Vec<Domain>> {
        let domains = self
            .domains
            .get_by_user(uid)
            .await
            .hide_err("Error getting user domains")?;

        if domains.is_empty() {
            self.domains
                .add(&AddDomain {
                    owner: uid.clone(),
                    domain: format!("{}.tardis.ac", **uid),
                })
                .await
                .hide_err("Error adding tardis.ac domain")?;

            self.domains
                .get_by_user(uid)
                .await
                .map(Into::into)
                .hide_err("Error getting user domains")
        } else {
            Ok(domains)
        }
    }

    pub async fn add_endpoint(&self, uid: &Username, endpoint: &NewEndpoint) -> Result<()> {
        match self.domains.belongs_to_user(&endpoint.domain, uid).await {
            Ok(true) => (),
            Ok(false) => bail!("That domain doesn't belong to you!"),
            Err(e) => return Err(log_hidden_err("Error checking domain ownership", e)),
        };

        match self.endpoints.add_endpoint(uid, endpoint).await {
            Ok(()) => Ok(()),
            Err(DBError::ConstraintFailed) => Err(anyhow!(
                "You already have an endpoint at that domain and prefix"
            )),
            Err(e) => Err(log_hidden_err("Error adding endpoint", e)),
        }
    }

    pub async fn get_all_endpoints(&self) -> Result<Vec<Endpoint>> {
        self.endpoints
            .get_all()
            .await
            .hide_err("Error getting all endpoints")
    }

    pub async fn delete_endpoint(&self, uid: &Username, prefix: &str, domain: &str) -> Result<()> {
        self.endpoints
            .delete_with_user(uid, prefix, domain)
            .await
            .hide_err("Error deleting endpoint")
    }
}
