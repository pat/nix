use crate::{adapters::DBError, application::errors::log_hidden_err, App};

use anyhow::{anyhow, Result};
use lettre::Address as EmailAddress;
use sonic_types::Password;

use super::errors::HideErrorExt;

impl App {
    pub async fn send_reset_pw_email(&self, email: &EmailAddress) -> Result<()> {
        if self.ldap.uid_from_email(email).await.is_err() {
            return Ok(());
        }

        let token = Password::gen_random();

        self.pw_resets
            .store_reset_token(email, &token)
            .await
            .hide_err("Error storing password reset token")?;

        self.email
            .send_reset_pw_email(email, &token)
            .await
            .hide_err("Error sending reset password email")?;

        Ok(())
    }

    pub async fn reset_pw_token_valid(&self, token: &Password) -> bool {
        self.pw_resets.get_token_email(token).await.is_ok()
    }

    pub async fn reset_password(&self, token: &Password, new_password: &Password) -> Result<()> {
        let email = match self.pw_resets.get_token_email(token).await {
            Ok(email) => email,
            Err(DBError::NotFound) => return Err(anyhow!("Invalid token")),
            Err(e) => {
                log_hidden_err("Error getting email for reset token", e);
                return Err(anyhow!("Internal error, please try again later."));
            }
        };

        let uid = self
            .ldap
            .uid_from_email(&email)
            .await
            .hide_err("Error getting uid from email")?;

        if let Some(uid) = uid {
            self.krb
                .reset_password(&uid, new_password)
                .await
                .hide_err("Error resetting password")?;
        }

        self.pw_resets
            .remove_token(token)
            .await
            .hide_err("Error removing token after use")?;

        Ok(())
    }
}
