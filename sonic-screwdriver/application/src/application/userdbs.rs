use anyhow::{bail, Result};
use sonic_types::{userdb::UserDB, Password, Username};

use crate::{adapters::DBError, App};

use super::errors::{log_hidden_err, HideErrorExt};

impl App {
    pub async fn userdbs_for_user(&self, uid: &Username) -> Result<Vec<UserDB>> {
        self.userdbs
            .get_by_user(uid)
            .await
            .map(Into::into)
            .hide_err("Error getting userdbs for user")
    }

    pub async fn add_userdb(&self, userdb: &UserDB) -> Result<Password> {
        match self.userdbs.create(userdb).await {
            Ok(()) => (),
            Err(DBError::ConstraintFailed) => bail!("You already have a database with that name!"),
            e => e.hide_err("Error creating userdb")?,
        };

        match self.userdbs.provision(userdb).await {
            Ok(pass) => Ok(pass),
            Err(e) => {
                let err_1 = log_hidden_err("Error provisioning userdb", e);

                // This might return, but we've already logged the error so that's fine.
                self.userdbs
                    .delete(userdb)
                    .await
                    .hide_err("Error deleting userdb after error provisioning")?;

                Err(err_1)
            }
        }
    }

    pub async fn reset_userdb_password(&self, userdb: &UserDB) -> Result<Password> {
        if !self.userdbs_for_user(&userdb.owner).await?.contains(userdb) {
            bail!("Database does not exist");
        }

        self.userdbs
            .reset_password(userdb)
            .await
            .hide_err("Error reseting password for userdb")
    }

    pub async fn delete_userdb(&self, userdb: &UserDB) -> Result<()> {
        self.userdbs
            .delete(userdb)
            .await
            .hide_err("Error deleting userdb")?;

        self.userdbs
            .unprovision(userdb)
            .await
            .hide_err("Error unprovisioning userdb")?;

        Ok(())
    }
}
