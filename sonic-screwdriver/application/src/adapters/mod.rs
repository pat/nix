pub mod applications;
mod discord;
mod domains;
mod email;
mod endpoints;
mod errors;
mod gitlab;
mod k8s;
mod krb_local;
pub mod ldap;
pub mod pw_resets;
mod userdbs;

pub use applications::ApplicationsAdapter;
pub use discord::DiscordAdapter;
pub use domains::DomainsAdapter;
pub use email::{EmailsAdapter, OurSmtpTransport};
pub use endpoints::EndpointsAdapter;
pub use errors::*;
pub use gitlab::GitlabAdapter;
pub use k8s::KubernetesAdapter;
pub use krb_local::LocalKerberosAdapter;
pub use pw_resets::PwResetsAdapter;
pub use userdbs::UserDBsAdapter;

pub type Pool = sqlx::SqlitePool;
