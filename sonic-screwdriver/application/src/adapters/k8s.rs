use std::collections::HashMap;

use anyhow::Result;
use reqwest::{tls::Certificate, Client, StatusCode};
use serde_json::json;

use crate::config::Kubernetes as KubernetesConfig;

pub struct KubernetesAdapter {
    conf: KubernetesConfig,
    client: Client,
}

impl KubernetesAdapter {
    pub fn new(conf: &KubernetesConfig) -> Result<Self> {
        Ok(Self {
            client: Client::builder()
                .add_root_certificate(Certificate::from_pem(conf.ca_cert.as_bytes())?)
                .build()?,
            conf: conf.clone(),
        })
    }

    /// Check if a namespace with the given name exists.
    pub async fn namespace_exists(&self, name: &str) -> Result<bool> {
        let resp = self
            .client
            .get(format!("{}/api/v1/namespaces/{}", self.conf.url, name))
            .bearer_auth(&self.conf.token)
            .send()
            .await?;

        if resp.status() == StatusCode::NOT_FOUND {
            Ok(false)
        } else {
            resp.error_for_status()?;
            Ok(true)
        }
    }

    /// Create a new namespace with the given name and labels.
    /// Returns Ok(true) if the namespace was created, Ok(false) if it already existed, or error in other cases.
    /// If this returns Ok(false), it is not guaranteed that the labels match.
    pub async fn create_namespace(&self, name: &str, labels: HashMap<&str, &str>) -> Result<bool> {
        let resp = self
            .client
            .post(format!("{}/api/v1/namespaces", &self.conf.url))
            .bearer_auth(&self.conf.token)
            .body(
                serde_json::to_string(&json!({
                    "apiVersion": "v1",
                    "kind": "Namespace",
                    "metadata": {
                        "name": name,
                        "labels":labels
                    }
                }))
                .unwrap(),
            )
            .send()
            .await?;

        if resp.status() == StatusCode::CONFLICT {
            return Ok(false); // Namespace already exists
        }

        resp.error_for_status()?;

        // kyverno deals with all the rest - see the tardis/k8s repo!
        Ok(true)
    }

    pub async fn delete_namespace(&self, name: &str) -> Result<()> {
        let resp = self
            .client
            .delete(format!("{}/api/v1/namespaces/{}", self.conf.url, name))
            .bearer_auth(&self.conf.token)
            .send()
            .await?;

        if resp.status() == StatusCode::NOT_FOUND {
            return Ok(()); // Namespace already deleted
        }

        resp.error_for_status()?;

        Ok(())
    }
}

// pub async fn create_namespace(config: &Config, username: &str) -> Result<()> {
//     let client = build_client(config)?;
//     let resp = client
//         .post(format!("{}/api/v1/namespaces", config.kubernetes.url))
//         .bearer_auth(&config.kubernetes.token)
//         .body(
//             serde_json::to_string(&json!({
//                 "apiVersion": "v1",
//                 "kind": "Namespace",
//                 "metadata": {
//                     "name": username,
//                     "labels": {
//                         "tardisproject.uk/owner": username,
//                     }
//                 }
//             }))
//             .unwrap(),
//         )
//         .send()
//         .await?;

//     if resp.status() == StatusCode::CONFLICT {
//         return Ok(()); // Namespace already exists
//     }

//     resp.error_for_status()?;

//     // kyverno deals with all the rest - see the tardis/k8s repo!
//     Ok(())
// }
// pub async fn remove_namespace(config: &Config, username: &str) -> Result<()> {
//     let client = build_client(config)?;
//     let resp = client
//         .delete(format!(
//             "{}/api/v1/namespaces/{}",
//             config.kubernetes.url, username
//         ))
//         .bearer_auth(&config.kubernetes.token)
//         .send()
//         .await?;

//     if resp.status() == StatusCode::NOT_FOUND {
//         return Ok(()); // Namespace already deleted
//     }

//     resp.error_for_status()?;

//     Ok(())
// }

// pub async fn namespace_exists(config: &Config, username: &str) -> Result<bool> {
//     let client = build_client(config)?;
//     let resp = client
//         .get(format!(
//             "{}/api/v1/namespaces/{}",
//             config.kubernetes.url, username
//         ))
//         .bearer_auth(&config.kubernetes.token)
//         .send()
//         .await?;

//     if resp.status() == StatusCode::NOT_FOUND {
//         Ok(false)
//     } else {
//         resp.error_for_status()?;
//         Ok(true)
//     }
// }
