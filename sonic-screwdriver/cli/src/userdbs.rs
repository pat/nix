use anyhow::{Context, Result};
use clap::Subcommand;
use dialoguer::Confirm;
use sonic_types::userdb::DBName;
use sonic_types::web::userdbs::{ManageUserDBRequest, UserDBConnectionDetails};

use crate::http::Client;
use log::*;

#[derive(Debug, Subcommand)]
pub enum Commands {
    #[command(visible_alias = "ls")]
    /// List your current managed databases
    List,

    #[command(visible_alias = "c", alias = "new", alias = "n")]
    /// Create a new managed database
    Create { name: String },

    #[command(visible_alias = "rpw", alias = "reset")]
    /// Reset the password for an existing managed database
    ResetPassword { name: String },

    /// Delete a managed database
    Delete { name: String },
}

pub fn dispatch(cmd: Commands) -> Result<()> {
    match cmd {
        Commands::List => list(),
        Commands::Create { name } => create(name),
        Commands::ResetPassword { name } => reset_pw(name),
        Commands::Delete { name } => delete(name),
    }
}

fn list() -> Result<()> {
    let client = Client::new_authenticated().context("Error creating HTTP client")?;

    let userdbs = client
        .list_userdbs()
        .context("Error fetching user databases")?;

    info!("Found {} databases\n", userdbs.len());
    for userdb in userdbs.iter() {
        println!("{}", &*userdb.name);
    }

    Ok(())
}

fn create(name: String) -> Result<()> {
    let name: DBName = name.try_into().context("Invalid database name")?;

    let client = Client::new_authenticated().context("Error creating HTTP client")?;
    let details = client
        .create_userdb(&ManageUserDBRequest {
            name: name.into_inner(),
        })
        .context("Error creating user database")?;

    info!("Created database successfully!");
    info!("{}", format_connection_details(&details));

    Ok(())
}

fn reset_pw(name: String) -> Result<()> {
    let name: DBName = name.try_into().context("Invalid database name")?;

    let client = Client::new_authenticated().context("Error creating HTTP client")?;
    let details = client
        .reset_userdb(&ManageUserDBRequest {
            name: name.into_inner(),
        })
        .context("Error resetting user database password")?;

    info!("Reset database details.");
    info!("{}", format_connection_details(&details));

    Ok(())
}

fn delete(name: String) -> Result<()> {
    let name: DBName = name.try_into().context("Invalid database name")?;

    if !Confirm::new()
        .with_prompt(&format!(
            "You will lose all data in {} permanently. Continue?",
            &*name
        ))
        .interact()?
    {
        return Ok(());
    }

    let client = Client::new_authenticated().context("Error creating HTTP client")?;
    client
        .delete_userdb(&ManageUserDBRequest {
            name: name.into_inner(),
        })
        .context("Error deleting user database")?;

    info!("Deleted database successfully.");

    Ok(())
}

fn format_connection_details(details: &UserDBConnectionDetails) -> String {
    format!(
        r#"Host: {}
Port: {}
Database: {}
Username: {}
Password: {}

You'll only see this password once, so store it somewhere secure.
"#,
        details.hostname,
        details.port,
        details.database,
        details.username,
        details.password.secret(),
    )
}
