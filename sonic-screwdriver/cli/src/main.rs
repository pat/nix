//! TARDIS user utilities.
mod endpoints;
mod http;
mod userdbs;

use anyhow::Result;
use clap::{Parser, Subcommand};
use env_logger::fmt::Color;
use log::Level;

use std::io::Write;

#[derive(Debug, Parser)]
#[command(name = "Sonic Screwdriver")]
#[command(version, about)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Manage hosted databases
    #[command(visible_alias = "db", alias = "dbs", alias = "database")]
    Databases {
        #[command(subcommand)]
        command: userdbs::Commands,
    },
    /// Manage endpoints
    #[command(visible_alias = "ep", alias = "eps", alias = "endpoint")]
    Endpoints {
        #[command(subcommand)]
        command: endpoints::Commands,
    },
}

fn main() -> Result<()> {
    let opts = Cli::parse();
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info"))
        .format(|buf, record| {
            let mut style = buf.style();

            match record.level() {
                Level::Error => style.set_color(Color::Red),
                Level::Warn => style.set_color(Color::Yellow),
                Level::Info => style.set_color(Color::Green),
                Level::Debug => style.set_color(Color::Blue),
                Level::Trace => style.set_color(Color::Black),
            };

            writeln!(buf, "{} {}", style.value(record.level()), record.args())
        })
        .init();

    match opts.command {
        Commands::Databases { command } => userdbs::dispatch(command),
        Commands::Endpoints { command } => endpoints::dispatch(command),
    }?;

    Ok(())
}
