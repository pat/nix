use std::{collections::HashMap, convert::Infallible, io::Cursor, sync::Arc};

use anyhow::{anyhow, Result};
use axum::{
    async_trait,
    extract::{FromRequestParts, State},
    http::StatusCode,
    response::{IntoResponse, IntoResponseParts},
    routing::get,
};
use axum_extra::extract::{
    cookie::{Cookie, SameSite},
    CookieJar,
};
use captcha::{
    filters::{Noise, Wave},
    Captcha,
};
use hound::{WavReader, WavWriter};
use rand::{distributions::Alphanumeric, Rng};
use time::{Duration, OffsetDateTime};
use tokio::sync::Mutex;

use crate::state::{AppState, Router};

#[derive(Debug, Default)]
pub struct CaptchaStore(Mutex<HashMap<String, (OffsetDateTime, Vec<char>)>>);

const COOKIE_NAME: &str = "captcha";
const N_CHARS: u32 = 5;

const SOFT_MAX_SIZE: usize = 2500;
const MAX_SIZE: usize = 5000;

pub fn routes() -> Router {
    Router::new()
        .route("/captcha.png", get(get_captcha))
        .route("/captcha.wav", get(get_captcha_audio))
}

pub async fn get_captcha(
    cookies: CookieJar,
    State(captcha_store): State<Arc<CaptchaStore>>,
) -> impl IntoResponse {
    match captcha_store.captcha_bytes(&cookies).await {
        Some(bytes) => (StatusCode::OK, [("Content-Type", "image/png")], bytes).into_response(),
        None => (
            StatusCode::BAD_REQUEST,
            [("Content-Type", "text/plain")],
            "Captcha not previously requested",
        )
            .into_response(),
    }
}

pub async fn get_captcha_audio(
    cookies: CookieJar,
    State(captcha_store): State<Arc<CaptchaStore>>,
) -> impl IntoResponse {
    match captcha_store.captcha_bytes_audio(&cookies).await {
        Ok(bytes) => (StatusCode::OK, [("Content-Type", "audio/wav")], bytes).into_response(),
        Err(_) => (
            StatusCode::BAD_REQUEST,
            [("Content-Type", "text/plain")],
            "Captcha not previously requested",
        )
            .into_response(),
    }
}

impl CaptchaStore {
    pub async fn new_captcha(&self) -> Cookie<'static> {
        let key: String = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(20)
            .map(char::from)
            .collect();

        self.old_check().await;

        Cookie::build(COOKIE_NAME, key)
            .max_age(Duration::minutes(10))
            .same_site(SameSite::Lax)
            .finish()
    }

    pub async fn old_check(&self) {
        let mut captchas = self.0.lock().await;
        if captchas.len() > SOFT_MAX_SIZE {
            let now = OffsetDateTime::now_utc();
            captchas.retain(|_, (e, _)| *e > now);
        }
        if captchas.len() > MAX_SIZE {
            // The collect is needed because of the borrow checker
            // #![allow(clippy::needless_collect)]

            let mut expiries: Vec<_> = captchas.iter().map(|(k, (e, _))| (k, e)).collect();
            expiries.sort_by(|(_, e1), (_, e2)| e1.cmp(e2));

            let surplus = captchas.len() - MAX_SIZE;
            let expiries: Vec<_> = expiries
                .into_iter()
                .take(surplus)
                .map(|(k, _)| k.to_owned())
                .collect();

            expiries.into_iter().for_each(|k| {
                captchas.remove(&k);
            });
        }
    }

    pub async fn captcha_bytes_audio(&self, cookies: &CookieJar) -> Result<Vec<u8>> {
        let key = cookies
            .get(COOKIE_NAME)
            .ok_or_else(|| anyhow!("no registered captcha"))?
            .value();

        let (chars, wavs) = {
            let mut captcha = Captcha::new();
            captcha.add_chars(N_CHARS);

            (captcha.chars(), captcha.as_wav())
        };

        let bytes = {
            if wavs.is_empty() {
                return Err(anyhow!("no wav files found"));
            }
            let mut bytes = Vec::new();
            let spec = {
                let bytes = &wavs[0]
                    .as_ref()
                    .ok_or_else(|| anyhow!("no wav files found"))?;
                let sample_reader = WavReader::new(bytes.as_slice())?;
                sample_reader.spec()
            };

            let mut sample_writer = WavWriter::new(Cursor::new(&mut bytes), spec)?;

            for audio in wavs.into_iter().flatten() {
                let mut sample_reader = WavReader::new(audio.as_slice())?;
                for sample in sample_reader.samples::<i16>() {
                    sample_writer.write_sample(sample?)?;
                }
            }
            sample_writer.flush()?;
            drop(sample_writer);

            bytes
        };

        let expiry = OffsetDateTime::now_utc() + Duration::minutes(10);
        self.0.lock().await.insert(key.to_string(), (expiry, chars));

        Ok(bytes)
    }

    pub async fn captcha_bytes(&self, cookies: &CookieJar) -> Option<Vec<u8>> {
        let key = cookies.get(COOKIE_NAME)?.value();

        let (chars, png) = {
            let mut captcha = Captcha::new();
            captcha
                .add_chars(N_CHARS)
                .apply_filter(Noise::new(0.4))
                .apply_filter(Wave::new(2.0, 20.0).horizontal())
                .apply_filter(Wave::new(2.0, 20.0).vertical())
                .view(220, 120);

            (captcha.chars(), captcha.as_png())
        };

        let expiry = OffsetDateTime::now_utc() + Duration::minutes(10);
        self.0.lock().await.insert(key.to_string(), (expiry, chars));

        png
    }

    pub async fn verify_captcha(&self, cookies: &CookieJar, answer: &str) -> bool {
        let key = match cookies.get(COOKIE_NAME) {
            Some(cookie) => cookie.value(),
            None => return false,
        };
        let (expiry, chars) = match self.0.lock().await.remove(key) {
            Some(data) => data,
            None => return false,
        };

        expiry > OffsetDateTime::now_utc() && answer.chars().zip(chars.iter()).all(|(a, e)| a == *e)
    }
}

/// Extractor and response part to issue a new captcha.
/// The captcha is constructed upon extraction, and must then be returned for the client to actually see it.
pub struct IssueNewCaptcha(CookieJar);

#[async_trait]
impl FromRequestParts<AppState> for IssueNewCaptcha {
    type Rejection = Infallible;

    async fn from_request_parts(
        parts: &mut axum::http::request::Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let cookies = CookieJar::from_request_parts(parts, state).await?;

        Ok(Self(cookies.add(state.captcha.new_captcha().await)))
    }
}
impl IntoResponseParts for IssueNewCaptcha {
    type Error = Infallible;

    fn into_response_parts(
        self,
        res: axum::response::ResponseParts,
    ) -> Result<axum::response::ResponseParts, Self::Error> {
        self.0.into_response_parts(res)
    }
}
