use std::sync::Arc;

use axum::{
    extract::{Path, State},
    response::{IntoResponse, Redirect},
    routing::get,
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;
use sonic_types::pw_reset::{PerformResetPwRequest, ResetPwRequest};

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};

pub fn routes() -> Router {
    Router::new()
        .route("/pwreset", get(form).post(submit))
        .route("/pwreset/:token", get(perform_form).post(perform_submit))
}

async fn form(flashes: IncomingFlashes) -> impl IntoResponse {
    let templ = render(
        "reset_password.html",
        context! {
            msgs: flashes.for_template()
        },
    );

    (flashes, templ)
}

async fn submit(
    flash: Flash,
    State(app): State<Arc<App>>,
    Form(reset_request): Form<ResetPwRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let email = reset_request
        .email
        .try_into()
        .err_to_redirect(&flash, "/pwreset")?;

    app.send_reset_pw_email(&email)
        .await
        .err_to_redirect(&flash, "/pwreset")?;

    Ok(render("reset_password_sent.html", context! {}))
}

async fn perform_form(
    flashes: IncomingFlashes,
    Path(token): Path<String>,
    State(app): State<Arc<App>>,
) -> Result<impl IntoResponse, impl IntoResponse> {
    let Ok(token) = token.try_into() else {
        return Err(render(
            "perform_reset_password.html",
            context! {
                token_invalid: true
            },
        ));
    };

    if !app.reset_pw_token_valid(&token).await {
        return Err(render(
            "perform_reset_password.html",
            context! {
                token_invalid: true
            },
        ));
    }

    let templ = render(
        "perform_reset_password.html",
        context! {
            msgs: flashes.for_template()
        },
    );

    Ok((flashes, templ))
}

async fn perform_submit(
    flash: Flash,
    Path(token): Path<String>,
    State(app): State<Arc<App>>,
    Form(update): Form<PerformResetPwRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let redirect_url = format!("/pwreset/{}", token);
    if update.password.is_empty() {
        return Err((
            flash.error("Neither password can be empty"),
            Redirect::to(&redirect_url),
        ));
    }

    if update.password != update.confirm_password {
        return Err((
            flash.error("New passwords do not match"),
            Redirect::to(&redirect_url),
        ));
    }

    let token = token.try_into().err_to_redirect(&flash, &redirect_url)?;
    let new_password = &update
        .password
        .try_into()
        .err_to_redirect(&flash, &redirect_url)?;

    app.reset_password(&token, new_password)
        .await
        .err_to_redirect(&flash, &redirect_url)?;

    Ok(render(
        "perform_reset_password.html",
        context! {
            success: true
        },
    ))
}
