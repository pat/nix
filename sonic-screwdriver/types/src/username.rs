use std::{error::Error, fmt::Display, ops::Deref};

use serde::{Deserialize, Serialize};

/// A wrapper around a username, which is guaranteed to be of the correct format.
/// This uses the type system to guarantee we've checked validity
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Clone)]
pub struct Username(String);

impl Display for Username {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Deref for Username {
    type Target = String; // TODO: change to str

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TryFrom<String> for Username {
    type Error = InvalidUsernameError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.is_empty() || value.len() > 32 {
            return Err(InvalidUsernameError::WrongLength);
        }

        if !value.chars().all(|c| match c {
            '-' | '_' => true,
            x if x.is_ascii_lowercase() => true,
            x if x.is_ascii_digit() => true,
            _ => false,
        }) {
            return Err(InvalidUsernameError::WrongCharacters);
        }

        Ok(Username(value))
    }
}

#[derive(Debug, Clone, Copy)]
pub enum InvalidUsernameError {
    WrongLength,
    WrongCharacters,
}

impl Error for InvalidUsernameError {}
impl Display for InvalidUsernameError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::WrongLength => write!(f, "Must be between 1 and 32 characters"),
            Self::WrongCharacters => write!(
                f,
                "Must contain only lowercase alphanumeric characters and -_"
            ),
        }
    }
}
