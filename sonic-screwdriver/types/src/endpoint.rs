use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};

use std::{fmt::Display, net::Ipv4Addr, ops::Deref, str::FromStr};

use super::Username;

#[derive(Debug, Serialize, Deserialize)]
pub struct Endpoint {
    pub domain: Domain,
    pub prefix: PathPrefix,
    pub target_ip: InternalIpAddr,
    pub target_port: u16,
    pub target_is_https: bool,

    pub strip_prefix: bool,
    pub force_https: bool,
    pub host_override: Option<String>,
}

impl Endpoint {
    /// Get a link to the endpoint
    pub fn link(&self) -> String {
        format!("https://{}{}", self.domain.domain, &*self.prefix)
    }

    /// Display the destination as a link
    pub fn dest_display(&self) -> String {
        if self.target_is_https {
            format!("https://{}:{}", &*self.target_ip, self.target_port)
        } else {
            format!("http://{}:{}", &*self.target_ip, self.target_port)
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewEndpoint {
    pub domain: String,
    pub prefix: PathPrefix,
    pub target_ip: InternalIpAddr,
    pub target_port: u16,
    pub target_is_https: bool,

    pub strip_prefix: bool,
    pub force_https: bool,
    pub host_override: Option<String>,
}

impl NewEndpoint {
    /// Get a link to the endpoint
    pub fn link(&self) -> String {
        format!("https://{}{}", self.domain, &*self.prefix)
    }

    /// Display the destination as a link
    pub fn dest_display(&self) -> String {
        if self.target_is_https {
            format!("https://{}:{}", &*self.target_ip, self.target_port)
        } else {
            format!("http://{}:{}", &*self.target_ip, self.target_port)
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Domain {
    pub id: i32,
    pub owner: Username,
    pub domain: String,
}

impl Domain {
    pub fn into_domain(self) -> String {
        self.domain
    }

    pub fn is_custom(&self) -> bool {
        self.domain != format!("{}.tardis.ac", self.owner)
    }
}

#[derive(Debug)]
pub struct AddDomain {
    pub owner: Username,
    pub domain: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PathPrefix(String);

impl PathPrefix {
    pub fn into_inner(self) -> String {
        self.0
    }
}

impl From<String> for PathPrefix {
    fn from(s: String) -> Self {
        if !s.starts_with('/') {
            Self(format!("/{}", s))
        } else {
            Self(s)
        }
    }
}

impl Deref for PathPrefix {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InternalIpAddr(Ipv4Addr);
impl InternalIpAddr {
    pub fn new(addr: Ipv4Addr) -> Result<Self> {
        if addr.is_private() {
            Ok(InternalIpAddr(addr))
        } else {
            Err(anyhow!("IP address must be in the private ranges (192.168.0.0/24, 10.0.0.0/8, 172.16.0.0/12)"))
        }
    }
}

impl FromStr for InternalIpAddr {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::new(Ipv4Addr::from_str(s)?)
    }
}

impl Deref for InternalIpAddr {
    type Target = Ipv4Addr;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl Display for InternalIpAddr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
